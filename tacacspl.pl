#tacacspl.pl
#created by _KUL (Kuleshov)
#version 2016-05-16
#Apache License 2.0

#!C:\Strawberry\perl\bin\perl.exe
use Plack::Loader;
my $app = Plack::Util::load_psgi("tacacspl.psgi");
Plack::Loader->auto->run($app);