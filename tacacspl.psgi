#tacacspl.psgi
#created by _KUL (Kuleshov)
#version 2016-05-16
#Apache License 2.0

#!/bin/perl
use strict;
use warnings;
use utf8;
use v5.14;
use Plack;
use Plack::Request;
use Plack::Builder;

my ($req,$file,$fileisok,$refgmass,$mode,$locationdir,$html);

#главное приложение
my $app = sub {
	my $env = shift;
	$req = Plack::Request->new($env);
	$html .= q~<div style="font-size:10px; width:100%; text-align:right">Created by Kuleshov. Version 3_16.05.2016</div>~;
	if ($req->param('mode') eq "auth") {
		$locationdir = q~C:\\Apache2\\www-perl\\tacacspl\\log\\access\\~;
		$mode = "auth";
	} elsif ($req->param('mode') eq "log") {
		$locationdir = q~C:\\Apache2\\www-perl\\tacacspl\\log\\acct\\~;
		$mode = "log";
	} else {
		$html .= show_mode();
	}
	if ($locationdir) {
		$html .= qq~<a href="${$req->base}"><< назад</a><br><br>~;
		$html .= get_form_files($locationdir);
		$html .= get_form_params("$locationdir$file");
	}
	return [
		'200',
		['Content-Type' => 'text/html', 'Charset' => 'UTF-8'],
		[ get_html("top"), $html, get_html("bottom")],
	];
};

#функция выбора режима
sub show_mode {
	my $rawhtml = q~
				<table align=center>
					<tr>
						<td>
							<a href="?mode=log">Статистика событий</a>
						</td>
						<td width=10px>
						</td>
						<td color=>
							<a href="?mode=auth">Статистика авторизаций</a><br>
						</td>
					</tr>
				</table>
			~;
	return $rawhtml;
}

#функции главного приложения
sub get_form_files {
	my $dir = shift;
	my ($files,$rawhtml,$tmpstr,$flag);
	if (opendir(my $dh, $dir)) {
		while (my $unit = readdir($dh)) {
			if ($unit =~ m/.log/) {
				push (@{$files},$unit);
			}
		}
		if ($req->param('file')) {
			utf8::encode($file = $req->param('file'))
		};
		$rawhtml .= q~
					<div>
						<form action=tacacspl.pl method=GET>
							<select name="file">
					~;
		for my $i (sort {$b <=> $a} @{$files}) {
			$flag = "";
			$tmpstr = "";
			if ($i eq $file) {
				$tmpstr = " selected";
				$fileisok = 1;
			};
			$rawhtml .= "<option value=\"$i\"$tmpstr>$i</option>";
		}
		$rawhtml .= qq~
							</select>
							<input hidden type="input" name="mode" value="$mode">
							<input type="submit" value="Выбрать">
						</form>
					</div>
					~;
		return $rawhtml;
	} else {
		return "Невозможно открыть директорию $dir ($!)"
	};
}

sub get_form_params {
	my $f = shift;
	if ($fileisok == 1) {
		if (open (my $fh, "<", $f)) {
		
			#обрабатываем системные логи
			if ($mode eq "log") {
				my $refrawmass;
				my $count = 0;
				my ($refdevices,$refusers,$refsources,$refservices);
				my ($rawhtml,$tmp,$tmpstr,$flag);
				while (my $rawrow = <$fh>) {
					$refgmass->[$count]->{mass} = [split("\t",$rawrow)];
					$refgmass->[$count]->{countelem} = scalar @{$refgmass->[$count]->{mass}};
					if (!exists($refdevices->{$refgmass->[$count]->{mass}->[1]})) {
						$refdevices->{$refgmass->[$count]->{mass}->[1]} = 1;
					}
					if (!exists($refusers->{$refgmass->[$count]->{mass}->[2]})) {
						$refusers->{$refgmass->[$count]->{mass}->[2]} = 1;
					}
					if (!exists($refsources->{$refgmass->[$count]->{mass}->[4]})) {
						$refsources->{$refgmass->[$count]->{mass}->[4]} = 1;
					}
					if (!exists($refservices->{$refgmass->[$count]->{mass}->[8]})) {
						$refservices->{$refgmass->[$count]->{mass}->[8]} = 1;
					}
					++$count;
				}

				$rawhtml .= qq~
						<div style="margin-top:20px">
							<form action=tacacspl.pl method=GET>
							<input hidden name="mode" value="$mode">
							<input hidden name="file" value="$file">
							<input hidden name="action" value="show">
							<table>
						~;
				$rawhtml .= q~	<tr>
									<td>
										Устройство:
									</td>
									<td>
										<select name="device">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('device');
				for my $i (sort keys %{$refdevices}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Пользователь:
									</td>
									<td> 
										<select name="user">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('user');
				for my $i (sort keys %{$refusers}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Источник:
									</td>
									<td> 
										<select name="source">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('source');
				for my $i (sort keys %{$refsources}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Сервис:
									</td>
									<td> 
										<select name="service">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('service');
				for my $i (sort keys %{$refservices}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	</table>
								<i>Крайне не желательно оставлять абсолютно все фильтры пустыми!<br>
								Это вызывает большую нагрузку на сервер и Ваш браузер.</i><br>
								<input type="submit" value = "Сформировать">
							</form>
						</div>
						~;
				if ($req->param('action') eq "show") {
					$rawhtml .= get_content();
				};
				return $rawhtml;
				
			#обрабатываем логи авторизации	
			} 
			elsif ($mode eq "auth") {
				my $refrawmass;
				my $count = 0;
				my ($refdevices,$refusers,$refsources,$refresult);
				my ($rawhtml,$tmp,$tmpstr,$flag);
				while (my $rawrow = <$fh>) {
					my $tmparr1 = [split("\t",$rawrow)];
					$tmparr1->[1] =~ s/://m;
					my $tmparr2 = [split(" ",$tmparr1->[1])];
					my @tmparr;
					push (@tmparr, $tmparr1->[0]);
					push (@tmparr, @{$tmparr2});
					$refgmass->[$count]->{mass} = \@tmparr;
					$refgmass->[$count]->{countelem} = scalar @{$refgmass->[$count]->{mass}};
					#$rawhtml .= $refgmass->[$count]->{countelem}."<br>";
					if (!exists($refdevices->{$refgmass->[$count]->{mass}->[1]})) {
						$refdevices->{$refgmass->[$count]->{mass}->[1]} = 1;
					}
					if (!exists($refusers->{$refgmass->[$count]->{mass}->[5]})) {
						$refusers->{$refgmass->[$count]->{mass}->[5]} = 1;
					}
					if (!exists($refsources->{$refgmass->[$count]->{mass}->[7]})) {
						$refsources->{$refgmass->[$count]->{mass}->[7]} = 1;
					}
					if (!exists($refresult->{$refgmass->[$count]->{mass}->[8]})) {
						$refresult->{$refgmass->[$count]->{mass}->[10]} = 1;
					}
					++$count;
				}
				$rawhtml .= qq~
						<div style="margin-top:20px">
							<form action=tacacspl.pl method=GET>
							<input hidden name="mode" value="$mode">
							<input hidden name="file" value="$file">
							<input hidden name="action" value="show">
							<table>
						~;
				$rawhtml .= q~	<tr>
									<td>
										Устройство:
									</td>
									<td>
										<select name="device">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('device');
				for my $i (sort keys %{$refdevices}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Пользователь:
									</td>
									<td> 
										<select name="user">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('user');
				for my $i (sort keys %{$refusers}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Источник:
									</td>
									<td> 
										<select name="source">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('source');
				for my $i (sort keys %{$refsources}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	<tr>
									<td>
										Результат:
									</td>
									<td> 
										<select name="result">
										<option value="">без фильтра</option>
								~;
				$tmp = $req->param('result');
				for my $i (sort keys %{$refresult}) {
					$flag = "";
					$tmpstr = "";
					if ($i eq $tmp) {
						$tmpstr = " selected";
					};
					$rawhtml .= qq~<option value="$i"$tmpstr>$i</option>~;
				}
				$rawhtml .= q~
										</select>
									</td>
								</tr>
								~;
				$rawhtml .= q~	</table>
								<i>Крайне не желательно оставлять абсолютно все фильтры пустыми!<br>
								Это вызывает большую нагрузку на сервер и Ваш браузер.</i><br>
								<input type="submit" value = "Сформировать">
							</form>
						</div>
						~;
				
				if ($req->param('action') eq "show") {
					$rawhtml .= get_content();
				};
				return $rawhtml;
			}
		} else {
			return "Невозможно открыть файл $f ($!)";
		}
	}
}

sub get_content {
	my $rawhtml;
	if ($mode eq "log") {
		$rawhtml .= qq~	<br>
					<table border=0>
					<tr style=\"background:#7AE3A6"\">
					<td></td>
					<td><b>Устройство</b></td>
					<td><b>Пользователь</b></td>
					<td></td>
					<td><b>Источник</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td><b>Сервис</b></td>
					<td></td>
					<td></td>
					<td></td>
					</tr>
						~;
		my $colorflag = 0;
		my $colorrow;
		for my $tmp (@{$refgmass}) {
			my $displaying = 1;
			if ($req->param('device')) {
				if ($req->param('device') ne $tmp->{mass}->[1]) {$displaying = $displaying-1};
			}
			if ($req->param('user')) {
				if ($req->param('user') ne $tmp->{mass}->[2]) {$displaying = $displaying-1};
			}
			if ($req->param('source')) {
				if ($req->param('source') ne $tmp->{mass}->[4]) {$displaying = $displaying-1};
			}
			if ($req->param('service')) {
				if ($req->param('service') ne $tmp->{mass}->[8]) {$displaying = $displaying-1};
			}
			if ($displaying eq 1) {
				if ($colorflag eq 0) {
					$colorrow = "#DAFFEA";
					$colorflag = 1;
				} else {
					$colorrow = "#AEF2CA"; 
					$colorflag = 0;
				} ;
				$rawhtml .= qq~
								<tr style=\"background:$colorrow\">
									<td>
										$tmp->{mass}->[0]
									</td>
									<td>
										$tmp->{mass}->[1]
									</td>
									<td>
										$tmp->{mass}->[2]
									</td>
									<td>
										$tmp->{mass}->[3]
									</td>
									<td>
										$tmp->{mass}->[4]
									</td>
									<td>
										$tmp->{mass}->[5]
									</td>
									<td>
										$tmp->{mass}->[6]
									</td>
									<td>
										$tmp->{mass}->[7]
									</td>
									<td>
										$tmp->{mass}->[8]
									</td>
									<td>
										$tmp->{mass}->[9]
									</td>
									<td>
										$tmp->{mass}->[10]
									</td>
								~;
				if ($tmp->{countelem} > 10) {
					my $microtmpstr;
					for (my $mictrotmp = 11; $mictrotmp < $tmp->{countelem}; ++$mictrotmp) {
						$microtmpstr .= $tmp->{mass}->[$mictrotmp];
					}
					$rawhtml .= qq~
									<td>
										$microtmpstr
									</td>
									~;
				}
				$rawhtml .= qq~
								</tr>
								~;
			}
		}
		$rawhtml .= qq~
						</table>
						~;
	}
	elsif($mode eq "auth") {
		$rawhtml .= qq~	<br>
					<table border=0>
					<tr style=\"background:#ADC4F1"\">
					<td></td>
					<td><b>Устройство</b></td>
					<td></td>
					<td><b>Пользователь</b></td>
					<td><b>Источник</b></td>
					<td></td>
					<td><b>Результат</b></td>
					<td></td>
					</tr>
						~;
		my $colorflag = 0;
		my $colorrow;
		for my $tmp (@{$refgmass}) {
			my $displaying = 1;
			if ($req->param('device')) {
				if ($req->param('device') ne $tmp->{mass}->[1]) {$displaying = $displaying-1};
			}
			if ($req->param('user')) {
				if ($req->param('user') ne $tmp->{mass}->[5]) {$displaying = $displaying-1};
			}
			if ($req->param('source')) {
				if ($req->param('source') ne $tmp->{mass}->[7]) {$displaying = $displaying-1};
			}
			if ($req->param('result')) {
				if ($req->param('result') ne $tmp->{mass}->[10]) {$displaying = $displaying-1};
			}
			if ($displaying eq 1) {
				if ($colorflag eq 0) {
					$colorrow = "#E8F0FF";
					$colorflag = 1;
				} else {
					$colorrow = "#CDDEFF"; 
					$colorflag = 0;
				} ;
				$rawhtml .= qq~
								<tr style=\"background:$colorrow\">
									<td>
										$tmp->{mass}->[0]
									</td>
									<td>
										$tmp->{mass}->[1]
									</td>
									<td>
										$tmp->{mass}->[2]
									</td>
									<td>
										$tmp->{mass}->[5]
									</td>
									<td>
										$tmp->{mass}->[7]
									</td>
									<td>
										$tmp->{mass}->[9]
									</td>
									<td>
										$tmp->{mass}->[10]
									</td>
								~;
				if ($tmp->{countelem} > 10) {
					my $microtmpstr;
					for (my $mictrotmp = 11; $mictrotmp < $tmp->{countelem}; ++$mictrotmp) {
						$microtmpstr .= $tmp->{mass}->[$mictrotmp]." ";
					}
					$rawhtml .= qq~
									<td>
										$microtmpstr
									</td>
									~;
				}
				$rawhtml .= qq~
								</tr>
								~;
			}
		}
		$rawhtml .= qq~
						</table>
						~;
	}
	elsif ($mode eq "log") {
	
	}
	return $rawhtml;
}

sub get_html {
	my $flag = shift;
	if ($flag eq "top"){
		return q~
				<!DOCTYPE html>
					<html lang="ru">
					<head>
						<meta http-equiv=Content-Type content="text/html;charset=UTF-8">
						<meta property="og:locale" content="ru_RU">
						<meta property="og:site_name" content="TACACSPL">
						<title>TACACSPL</title>
					</head>
					<body>~;
	}
	if ($flag eq "bottom"){
		return q~
					</body>
				</html>
		~;
	}
}

#маршрутизатор
my $main_app = builder {
    mount "/" => builder { $app; };
};
